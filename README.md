# Project Enterprise Web Application: KLM
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.1.5.
Clone this project with `git clone git@gitlab.fdmci.hva.nl:kokr6/KLM.git`

## Frontend
1. Navigate to de frontend directory
2. Install node modules with `npm install`

### Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Build and deploy
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
This build can be deployed directly to your choice of static web hosting like apache or nginx.

### Customization
Any variables that need to be changed to work for you specific deployment will be in 
`frontend/src/environments/environment.prod.ts/`. In here you can specify where you deployed your backend.
You will also have to provide your own google maps api key.

## Backend
1. Navigate to de backend directory
2. Install node modules with `npm install`
3. Update ormconfig.js to match your install of MySQL, see (TypeOrm)[http://typeorm.io/#/using-ormconfig] for more details.
You might want to use different values on production.
4. Run `backend/aca_database_script.sql` on your MySQL instance to initialize the `aca` database with sample data and 3 basic accounts

### Development server
Run `npm compile` and `npm run start` to start the development server on your local machine with all default values.

### Build and deploy
Run `ng compile` to compile the project, this will be compiled to the dist folder. To deploy upload the following files and directories to your server that has node installed.
`package.json`, `ormconfig.js`, `/dist`. Next run `npm install --prod` in the same folder.

Next setup a number of environment variables. 
If you are putting this on production you must set the variable `JWT_SECRET`, this will be the secret used to encrypt jwt tokens that are used by clients to authenticate themselves.
set `NODE_ENV` to 'production' once you have setup a certificate using letsencrypt's certbot. You will want to set `CERTIFICATE_DIR` as well.
These two environment variables will be used to make the server use https encryption. 

The following variables can be set to further customize the server.
- `PORT`
- `NODE_ROOT`
- `EXPIRES_IN`

`PORT` can be used to set the network port that the server is listening on. By default the server listens on the port 3000.  
`NODE_ROOT` can be used to change the relative path for all the resources. By default this is set to ``.  
`EXPIRES_IN` can be used to change how long a JWT token will be valid (in seconds). This defaults to 12 hours.

Once you have set up your environment variables you can run `npm run start` to start the node server and make it available for use.

## Using the application
Each user has easy access to a specific part of the application. The following users have been added by default, 
but more can easily be added from the admin portion of the application.

| Username | Password | Roles  |
|:---------|:---------|:-------|
| admin    | root     | admin  |
| desk     | desk     | desk   |
| driver   | driver   | driver |

### Development
If both the frontend and backend are running with their default settings they should automatically connect and 
provide a quick way to test your current changes.