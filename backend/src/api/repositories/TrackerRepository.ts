import {Pair, Tracker, Vehicle} from '../models';
import {getRepository, Like, Repository} from 'typeorm';
import {CACHE_TIME} from './index';

/**
 * Holds all functions that access the database of trackers.
 *
 * @author Stefan Breetveld
 */
export module TrackerRepository {

  /**
   * Create A TypeORM repository for the tracker class.
   */
  function getTrackerRepo(): Repository<Tracker> {
    return getRepository(Tracker);
  }

  /**
   * Create A TypeORM repository for the Pair class.
   */
  function getPairRepo(): Repository<Pair> {
    return getRepository(Pair);
  }

  /**
   * Get the last events for the specified vehicle
   *
   * @param vehicle a vehicle with events
   * @param length the amount of history you want
   */
  export async function getLastEvents(vehicle: string | Vehicle, length: number = 1): Promise<Tracker[]> {
    return getTrackerRepo().find({where: {vehicle: vehicle}, take: length, order: {eventTime: 'DESC'}});
  }

  /**
   * Get the sensorIDs for which there is at least one event that has the given key, value pair.
   *
   * @param key the key
   * @param value the value
   */
  export async function getKeyValueSensorIDs(key: string, value: string): Promise<string[] | never> {
    return getPairRepo().createQueryBuilder('pair').select('DISTINCT tracker.vehicle', 'sensorID')
      .leftJoin('pair.tracker', 'tracker').where({key: Like(key), value: Like(value)}).cache(CACHE_TIME).getRawMany()
      .then(values => values.map(value1 => value1.sensorID));
  }
}
