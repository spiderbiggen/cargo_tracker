import {Vehicle} from '../models';
import {getRepository, Repository} from 'typeorm';
import {BadRequestError, HttpError} from '../../util';
import {TrackerRepository} from './TrackerRepository';
import {CargoRepository} from './CargoRepository';

/**
 * Holds all functions that access the database of vehicles.
 *
 * @author Stefan Breetveld
 */
export module VehicleRepository {

  /**
   * Create A TypeORM repository for the vehicle class.
   */
  function getVehicleRepo(): Repository<Vehicle> {
    return getRepository(Vehicle);
  }

  /**
   * Filter the given Vehicles so that there is only one Tracker event and only cargo that isn't already checked out.
   *
   * @param vehicles the array of vehicles that needs to be filtered.
   * @param length the length of the retrieved history
   */
  async function filterVehicles(vehicles: Vehicle[], length: number = 1): Promise<Vehicle[]> {
    const fixedVehicles: Vehicle[] = [];
    for (const vehicle of vehicles) {
      const vehicle2 = await filterCargo(await filterTrackers(vehicle, length));
      if (vehicle2) {
        fixedVehicles.push(vehicle2);
      }
    }
    return fixedVehicles;
  }

  /**
   * Filter the trackers so that we only keep the last n trackers sorted by event time.
   *
   * @param vehicle the vehicle that has the list of trackers
   * @param length the amount of trackers that should be kept
   */
  async function filterTrackers(vehicle: Vehicle, length: number = 1): Promise<Vehicle | null> {
    return TrackerRepository.getLastEvents(vehicle, length).then(trackers => {
      vehicle.trackers = trackers;
      return vehicle;
    });
  }

  /**
   * Filter the cargo so that cargo that has already been checked out is not shown.
   *
   * @param vehicle the vehicle that has the list of cargo
   */
  async function filterCargo(vehicle: Vehicle | null): Promise<Vehicle | null> {
    if (!vehicle) { return vehicle; }
    vehicle.cargo = await CargoRepository.getCargo(vehicle).catch(() => undefined);
    return vehicle;
  }

  /**
   * Return all the vehicles with their respective relations.
   */
  export function getAllVehicles(): Promise<Vehicle[] | HttpError> {
    return getVehicleRepo().find()
      .then(async vehicles => await filterVehicles(vehicles));
  }

  /**
   * Return the Vehicle that corresponds to the given ID, and keep the last n Tracker events.
   *
   * @param sensorID the Vehicle we are looking for
   * @param length the amount of tracker events we want to keep.
   */
  export async function getByID(sensorID: string, length: number): Promise<Vehicle | HttpError> {
    return getVehicleRepo().findOneOrFail(sensorID).then(vehicle => filterVehicles([vehicle], length)).then(value => {
      if (value && value.length >= 1) {
        return value[0];
      }
      return new BadRequestError();
    });
  }

  /**
   * Get the Vehicles that have the given key and value in the extra data on the tracker.
   *
   * @param key the key of the key value pair
   * @param value the value of the key value pair
   */
  export async function getForKeyValuePair(key: string, value: string): Promise<Vehicle[] | HttpError> {
    const result = await TrackerRepository.getKeyValueSensorIDs(key, value);
    return getVehicleRepo().findByIds(result).then(vehicles => filterVehicles(vehicles));
  }

  /**
   * Check if the given sensorID corresponds to an actual vehicle.
   *
   * @param sensorID the possible sensorID of a vehicle.
   */
  export function checkExistence(sensorID: string): Promise<boolean> {
    return getVehicleRepo().findOneOrFail(sensorID).then(() => true, () => false);
  }

  /**
   * Return the vehicle stored in the database, without any of the relations
   *
   * @param sensorID the sensorID of a vehicle
   */
  export function getByIDNoRelations(sensorID: string): Promise<Vehicle | undefined> {
    return getVehicleRepo().findOne(sensorID);
  }
}
