import {Cargo, Vehicle} from '../models';
import {getRepository, Like, Repository} from 'typeorm';

/**
 * Holds all functions that access the database of cargo.
 *
 * @author Stefan Breetveld
 */
export module CargoRepository {

  /**
   * Create A TypeORM repository for the cargo class.
   */
  function getCargoRepo(): Repository<Cargo> {
    return getRepository(Cargo);
  }

  /**
   * Get a list of all cargo that has not been checked out.
   */
  export function getAllLinkedCargo(): Promise<Cargo[]> {
    return getCargoRepo().find({where: {checkOutTime: null}});
  }

  /**
   * find all cargo for a specific vehicle or check if the given uld is stored on the given vehicle.
   *
   * @param vehicleID a specific vehicle that can hold cargo.
   * @param uld a specific uld
   */
  export function getCargo(vehicleID: string | Vehicle, uld?: string): Promise<Cargo[]> {
    return getCargoRepo().find({where: {checkOutTime: null, vehicle: vehicleID, uld: Like(uld || '%')}});
  }

  /**
   * Save the given cargo object to the database.
   *
   * @param cargo a cargo object
   */
  export function storeCargo(cargo: Cargo): Promise<Cargo> {
    return getCargoRepo().save(cargo);
  }

  /**
   * Unlink the cargo specified by the given parameters.
   * If only a vehicle ID is given unlink all cargo from that vehicle.
   * If only a uld is given unlink
   *
   * @param vehicleID the vehicle that we want to unlink cargo from
   * @param uld the uld that should be unlinked
   */
  export async function unlinkCargo(vehicleID?: any, uld?: string): Promise<boolean> {
    if (!vehicleID && ! uld) {
      return false;
    }
    return getCargoRepo().update({
      vehicle: Like(vehicleID || '%'),
      uld: Like(uld || '%'),
      checkOutTime: null
    }, {checkOutTime: new Date()}).then(() => true).catch(() => false);
  }
}
