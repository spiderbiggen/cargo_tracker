import {User} from '../models';
import {compareSync, hash} from 'bcrypt';
import {getRepository, Repository, SelectQueryBuilder} from 'typeorm';
import {Authentication, BadRequestError, HttpError, TokenDetails, TokenPayload, UnauthorizedError} from '../../util';

/**
 * Holds all functions that access the database of vehicles.
 *
 * @author Stefan Breetveld
 */
export module UserRepository {

  const SALT_ROUNDS = 10;

  /**
   * Create A TypeORM repository for the User class.
   */
  function getUserRepo(): Repository<User> {
    return getRepository(User);
  }

  /**
   * Create a TypeORM query builder for the user class.
   */
  function getUserQB(): SelectQueryBuilder<User> {
    return getUserRepo().createQueryBuilder('user');
  }

  /**
   * Find the user with the given ID, if the user doesn't exist throw an error.
   *
   * @param id the user id.
   */
  export function getUserByID(id: string): Promise<User> {
    return getUserRepo().findOneOrFail(id);
  }

  /**
   * Check if there is a user with the given username and password.
   * If it exists return a new TokenPayload.
   *
   * @param username a user's username
   * @param password possibly that users password
   */
  export function authorize(username: string, password: string): Promise<TokenPayload | HttpError> {
    const query = getUserRepo().findOne({
      select: ['id', 'password', 'roles'],
      where: {username: username.toLowerCase()}
    });
    return query.then(user => {
      if (user !== undefined) {
        const value = compareSync(password, user.password || '');
        if (value) {
          const token = Authentication.signToken(new TokenDetails(user));
          return {'token': token, roles: user.roles};
        }
      }
      return new UnauthorizedError();
    });
  }

  /**
   * Update any of the specified information for the user with the given id,
   * All parameters are optional. Any parameters that are left blank will be kept as the old value.
   *
   * @param id the user we want to update
   * @param email the new email of the user
   * @param username the new username of the user
   * @param password the new password of the user
   * @param roles the new roles of the user
   */
  export async function updateUser(id?: string, email?: string, username?: string, password?: string, roles?: string[])
    : Promise<User | HttpError> {
    const oldUser = await getUserRepo().findOne(id);
    if (oldUser) {
      if (password) {
        const pHash = await hash(password, SALT_ROUNDS);
        oldUser.password = pHash || oldUser.password;
      }
      oldUser.email = email || oldUser.email;
      oldUser.username = (username || oldUser.username).toLowerCase();
      oldUser.roles = roles || oldUser.roles;
      return getUserRepo().save(oldUser).then(user => getUserRepo().findOneOrFail(user.id));
    }
    return new BadRequestError();
  }

  /**
   * Create a new user with the specified information. Throws an error if a user with the given email or username already exists.
   *
   * @param username the username of the new user
   * @param password the password of the new user
   * @param email the email of the new user
   * @param roles the roles of the new user
   */
  export async function createUser(username: string, password: string, email: string, roles?: string[]) {
    const _hash = await hash(password, SALT_ROUNDS);
    const repository = getUserRepo();
    const newUser = repository.create({username: username.toLowerCase(), password: _hash, email: email, roles: roles});
    return repository.save(newUser).then(user => getUserRepo().findOneOrFail(user.id));
  }

  /**
   * Return a paginated list of all users, can be filtered by roles and creation date.
   *
   * @param offset the paginated offset
   * @param limit the amount of users to retrieve
   * @param roles all users with all the given roles
   * @param minAge created at least minAge years ago
   * @param maxAge created at most maxAge years ago
   */
  export function getUsers(offset: number, limit: number, roles: string[], minAge: number, maxAge: number) {
    let query = getUserQB().select()
      .where('user.createdAt < DATE_SUB(NOW(), INTERVAL :minAge YEAR)', {minAge: minAge})
      .andWhere('user.createdAt > DATE_SUB(NOW(), INTERVAL :maxAge YEAR)', {maxAge: maxAge})
      .offset(offset).limit(limit);

    if (roles.length > 0) {
      const roleQuery = `user.roles LIKE CONCAT('%',:role${0},'%')`;
      query = query.andWhere(roleQuery, {[`role${0}`]: roles[0]});
      for (let i = 1; i < roles.length; i++) {
        query = query.orWhere(roleQuery, {[`role${i}`]: roles[i]});
      }
    }
    return query.getManyAndCount().then(value => ({count: value[1], users: value[0]}));
  }

}
