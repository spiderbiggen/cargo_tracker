import CargoRoutes from './CargoRoutes';
import UserRoutes from './UserRoutes';
import VehicleRoutes from './VehicleRoutes';
import {RouteWrapper} from '../../routing';

// export {CargoRoutes, UserRoutes};

/**
 * Export a list of all rou
 */
export const LIST: RouteWrapper[] = [CargoRoutes, UserRoutes, VehicleRoutes];
