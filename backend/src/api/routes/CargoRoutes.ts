import {CargoController} from '../controllers';
import {RouteWrapper} from '../../routing';
import {Authentication} from '../../util';

const router = new RouteWrapper('/cargo');

router.registerMiddleware(Authentication.verifyAuthentication);

router.get('/', CargoController.getAllCargo);
router.get('/:vehicleID', CargoController.getCargo);
router.post('/', CargoController.linkCargo);
router.delete('/', CargoController.unlinkCargo);

export default router;
