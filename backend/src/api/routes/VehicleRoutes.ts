import {VehicleController} from '../controllers';
import {RouteWrapper} from '../../routing';
import {Authentication} from '../../util';

const router = new RouteWrapper('/vehicles');

router.registerMiddleware(Authentication.verifyAuthentication);

router.get('/', VehicleController.getAll);
router.get('/:sensorID', VehicleController.getSensorID);
router.get('/check/:sensorID', VehicleController.checkIfVehicleExist);
router.get('/:key/:value', VehicleController.getForKeyValuePair);

export default router;
