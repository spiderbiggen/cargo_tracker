import {AdminController, UserController} from '../controllers';
import {RouteWrapper} from '../../routing';
import {Authentication} from '../../util';

const router = new RouteWrapper('/');

router.post('/authenticate', UserController.loginUser);

const authenticatedRouter = new RouteWrapper('/user');
authenticatedRouter.registerMiddleware(Authentication.verifyAuthentication);

authenticatedRouter.get('/', UserController.getUser);
authenticatedRouter.post('/', AdminController.updateUser);
authenticatedRouter.get('/many', AdminController.getUsers);

authenticatedRouter.get('/:id', AdminController.getUserByID);
router.subRoutes(authenticatedRouter);

export default router;
