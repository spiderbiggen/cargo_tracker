import {Request} from 'express';
import {Cargo} from '../models';
import {BadRequestError, DuplicateObjectError, HttpError, NotFoundError} from '../../util';
import {CargoRepository, VehicleRepository} from '../repositories';

/**
 * Module that has all resources for managing cargo.
 *
 * @author Stefan Breetveld
 */
export module CargoController {

  /**
   * Return all cargo listed on the specified vehicle.
   * Requires a :vehicleID parameter in the route.
   *
   * @param req the request sent by the client
   */
  export async function getCargo(req: Request): Promise<Cargo[] | HttpError> {
    const vehicleID = req.params.vehicleID;

    if (!vehicleID) {
      return new NotFoundError();
    }
    return CargoRepository.getCargo(vehicleID);
  }

  /**
   * Return all currently linked cargo.
   */
  export async function getAllCargo(): Promise<Cargo[] | HttpError> {
    return CargoRepository.getAllLinkedCargo();
  }

  /**
   * link a pallet(uld) to a vehicle. Send an error if it is already linked or if there are already
   * 2 pieces of cargo that are linked.
   *
   * @param req the request sent by the client
   */
  export async function linkCargo(req: Request): Promise<Cargo | HttpError> {
    const sensorID = req.body['palletCarId'];
    const uld = req.body['uld'];
    const date = req.body['checkInTime'] || new Date();
    if (!uld || !sensorID) {
      return new BadRequestError();
    }
    const vehicle = await VehicleRepository.getByIDNoRelations(sensorID);
    if (!vehicle) {
      return new BadRequestError(`${sensorID} is not a valid vehicle.`);
    }
    const cLink: Cargo = new Cargo(vehicle, uld, date);
    const result = await CargoRepository.getCargo(vehicle, uld);
    if (result.length !== 0) {
      return new DuplicateObjectError(result, 'duplicate');
    }
    const result2 = await CargoRepository.getCargo(vehicle);
    if (result2.length >= 2) {
      return new BadRequestError(`More than 2 pallets on ${sensorID}`);
    }
    return CargoRepository.storeCargo(cLink);
  }

  /**
   * Unlink the cargo specified by the query parameters palletCarId and uld.
   * If only a vehicle ID is given unlink all cargo from that vehicle.
   * If only a uld is given unlink
   *
   * @param req the request sent by the client
   */
  export async function unlinkCargo(req: Request): Promise<boolean | BadRequestError> {
    const uld = req.query.uld;
    const vehicleID = req.query.palletCarId;
    if (!uld && !vehicleID) {
      return new BadRequestError();
    }
    return CargoRepository.unlinkCargo(vehicleID, uld);
  }
}
