import {Request} from 'express';
import {Vehicle} from '../models';
import {HttpError, NotFoundError} from '../../util';
import {VehicleRepository} from '../repositories';

/**
 * Module that has all resources for managing vehicles.
 *
 * @author Stefan Breetveld
 */
export module VehicleController {

  /**
   * Return a list of all vehicles, with last location and current cargo.
   */
  export async function getAll(): Promise<Vehicle[] | HttpError> {
    return VehicleRepository.getAllVehicles();
  }

  /**
   * Return the vehicle specified by the sensorID in the route.
   * This vehicle has the last ?length tracker events and all the currently linked cargo.
   *
   * @param req the request sent by the client
   */
  export async function getSensorID(req: Request): Promise<Vehicle | HttpError> {
    const sensorID = req.params.sensorID;
    const length = req.query.length ? +req.query.length : 1;
    if (!sensorID) {
      return new NotFoundError();
    }
    return VehicleRepository.getByID(sensorID, length);
  }

  /**
   * Get the Vehicles that have the given key and value in the extra data on the tracker.
   *
   * @param req the request sent by the client.
   */
  export async function getForKeyValuePair(req: Request): Promise<Vehicle[] | HttpError> {
    const key = req.params.key;
    const value = req.params.value;
    if (!key || !value) {
      return new NotFoundError();
    }
    return VehicleRepository.getForKeyValuePair(key, value);
  }

  /**
   * Check if the given sensorID corresponds to an actual vehicle.
   *
   * @param req the request sent by the client.
   */
  export async function checkIfVehicleExist(req: Request): Promise<boolean | HttpError> {
    const sensorID = req.params.sensorID;
    if (!sensorID) {
      return new NotFoundError();
    }
    return VehicleRepository.checkExistence(sensorID);
  }
}
