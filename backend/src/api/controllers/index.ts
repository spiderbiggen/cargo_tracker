export {AdminController} from './AdminController';
export {UserController} from './UserController';
export {CargoController} from './CargoController';
export {VehicleController} from './VehicleController';

