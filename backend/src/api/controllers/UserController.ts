import {Request, Response} from 'express';
import {User} from '../models';
import {BadRequestError, HttpError, UnauthorizedError, TokenPayload} from '../../util';

import {UserRepository} from '../repositories';

/**
 * Module that has all resources for managing users.
 *
 * @author Stefan Breetveld
 */
export module UserController {

  /**
   * Get the user profile from the logged in user.
   *
   * @param req the request sent by the client
   * @param res Object where we can retrieve the user that is currently logged in.
   */
  export async function getUser(req: Request, res: Response): Promise<User | HttpError> {
    const user: User = res.locals.user;
    if (!user) {
      return new BadRequestError();
    }
    return UserRepository.getUserByID(user.id);
  }

  /**
   * Try to authenticate the user with the credentials stored in the body.
   *
   * {
   *     "username": "<username>",
   *     "password": "<password>"
   * }
   *
   * @param req the request sent by the client.
   */
  export async function loginUser(req: Request): Promise<TokenPayload | HttpError> {
    const username = req.body['username'];
    const password = req.body['password'];
    if (!username || !password) {
      return new UnauthorizedError();
    }
    return UserRepository.authorize(username, password);
  }
}
