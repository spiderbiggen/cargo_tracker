import {User} from '../models';
import {BadRequestError, HttpError, ForbiddenError, UnauthorizedError} from '../../util';
import {Request, Response} from 'express';
import {UserRepository} from '../repositories';

/**
 * Module that has all resources that will mainly be accessed by an admin user.
 *
 * @author Stefan Breetveld
 */
export module AdminController {

  const DEFAULT_PAGINATION_OFFSET = 0;
  const DEFAULT_PAGINATION_SIZE = 20;
  const MAX_PAGINATION_SIZE = 50;

  /**
   * Retrieves the details of a specified user if it has been requested by the user itself or by an admin user.
   * Requires a :id parameter to be in the route.
   *
   * @param req the request sent by the client
   * @param res Object where we can retrieve the user that is currently logged in.
   */
  export async function getUserByID(req: Request, res: Response): Promise<User | HttpError> {
    const user: User = res.locals.user;
    const id = req.params.id;
    if (!id) {
      return new BadRequestError();
    }
    if (!user.id === id && !user.isAdmin()) {
      return new UnauthorizedError();
    }
    return UserRepository.getUserByID(id);
  }

  /**
   * Updates or creates a specified user if it has been requested by the user itself or by an admin user.
   * Can create a new user when the query parameter create is set to true.
   * User information can be specified in the body
   *
   * {
   *     "id": "<id>",
   *     "email": "<email>",
   *     "username": "<username>",
   *     "password": "<password>",
   *     "roles": [<roles>]
   * }
   * When updating a specific user only the user id is required to be in the body all others are optional.
   *
   * @param req the request sent by the client
   * @param res Object where we can retrieve the user that is currently logged in.
   */
  export async function updateUser(req: Request, res: Response): Promise<User | HttpError> {
    const user: User = res.locals.user;
    const id = req.body['id'];
    const email = req.body['email'];
    const username = req.body['username'];
    const password = req.body['password'];
    const roles = req.body['roles'];
    if (user.id !== id && !user.isAdmin()) {
      return new ForbiddenError();
    }
    if (req.query.create === 'true') {
      return createUser(username, password, email, roles);
    }
    return UserRepository.updateUser(id, email, username, password, roles);
  }

  /**
   * Create a new user with the specified information.
   *
   * @param username the username of the new user
   * @param password the password of the new user
   * @param email the email of the new user
   * @param roles the roles of the new user
   */
  export async function createUser(username?: string, password?: string,
                                   email?: string, roles?: string[]): Promise<User | HttpError> {
    if (!email || !username || !password) {
      return new BadRequestError();
    }
    return UserRepository.createUser(username, password, email, roles);
  }

  /**
   * Return a paginated list of all users, can be filtered by roles and creation date.
   * Use query parameters to set the offset, limit roles, min_years and/or max_year.
   *
   * @param req the request sent by the client
   * @param res Object where we can retrieve the user that is currently logged in.
   */
  export async function getUsers(req: Request, res: Response): Promise<{ users: User[], count: number } | HttpError> {
    const user: User = res.locals.user;
    if (!user.isAdmin()) {
      return new ForbiddenError('Not an admin');
    }
    const offset = req.query.offset || DEFAULT_PAGINATION_OFFSET;
    const limit = Math.min(req.query.limit || DEFAULT_PAGINATION_SIZE, MAX_PAGINATION_SIZE);
    const roles = req.query.roles ? req.query.roles.split(',') : [];
    const minAge = req.query.min_years || -1000;
    const maxAge = req.query.max_years || 1000;
    return UserRepository.getUsers(offset, limit, roles, minAge, maxAge);
  }
}
