import {Column, Entity, OneToMany, PrimaryColumn} from 'typeorm';
import {Tracker} from './Tracker';
import {Cargo} from './Cargo';

/**
 * Entity that holds all data about a vehicle and has relations with trackers and cargo.
 *
 * @author Stefan Breetveld
 */
@Entity()
export class Vehicle {

  @PrimaryColumn()
  sensorID: string;
  @Column({nullable: true})
  gsEquipmentID?: string;
  @OneToMany(() => Tracker, tracker => tracker.vehicle, {eager: false})
  trackers: Tracker[];
  @OneToMany(() => Cargo, cargo => cargo.vehicle, {eager: false})
  cargo?: Cargo[];

  toJSON() {
    const obj: any = {
      sensorID: this.sensorID,
      gsEquipmentID: this.gsEquipmentID,
      cargo: this.cargo
    };
    if (this.trackers && this.trackers.length > 0) {
      obj.location = this.trackers[0];
    }
    return obj;
  }
}
