import {Column, Entity, ManyToOne, PrimaryColumn} from 'typeorm';
import {Tracker} from './Tracker';

/**
 * An entity that holds a key value pair has a relation with a specific tracker event.
 *
 * @author Stefan Breetveld
 */
@Entity()
export class Pair {

  @ManyToOne(() => Tracker, tracker => tracker.data, {primary: true})
  tracker: Tracker;

  @PrimaryColumn()
  key: string;

  @Column()
  value: string;

  toJSON() {
    return {[this.key]: this.value};
  }
}
