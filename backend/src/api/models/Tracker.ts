import {Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn, Unique} from 'typeorm';
import {Pair} from './Pair';
import {Vehicle} from './Vehicle';

/**
 * Entity that holds information about a tracker. The combination of sensorID eventTime and eventID needs to be unique.
 * has a relation with Key Value pairs and a vehicle object.
 *
 * @author Stefan Breetveld
 */
@Entity()
@Unique(['vehicle.sensorID', 'eventTime', 'eventID'])
export class Tracker {

  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  eventTime: Date;
  @Column({nullable: true})
  eventID?: string;
  @Column('double')
  latitude: number;
  @Column('double')
  longitude: number;
  @Column('double', {nullable: true})
  elevation?: number;
  @OneToMany(() => Pair, pair => pair.tracker, {eager: true})
  data: Pair[];
  @ManyToOne(() => Vehicle, vehicle => vehicle.trackers, {eager: true})
  vehicle: Vehicle;

  toJSON() {
    const obj: any = {
      sensorID: this.vehicle ? this.vehicle.sensorID : undefined,
      eventID: this.eventID,
      gsEquipmentID: this.vehicle ? this.vehicle.gsEquipmentID : undefined,
      eventTime: this.eventTime,
      longitude: this.longitude,
      latitude: this.latitude,
      elevation: this.elevation,
      data: {}
    };
    if (this.data) {
      for (const it of this.data) {
        obj.data[it.key] = it.value;
      }
    }
    return obj;
  }
}
