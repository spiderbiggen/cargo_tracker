import {Column, CreateDateColumn, Entity, PrimaryGeneratedColumn} from 'typeorm';

/**
 * Entity that holds information about a user.
 *
 * @author Stefan Breetveld
 */
@Entity()
export class User {

  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column({unique: true})
  username: string;
  @Column({unique: true})
  email: string;
  @Column({select: false})
  password?: string; // technically a hash of the password.
  @CreateDateColumn()
  createdAt?: Date;
  @Column('simple-array', {nullable: true})
  roles?: string[];

  isAdmin(): boolean {
    if (!this.roles) {
      return false;
    }
    return this.roles.indexOf('admin') >= 0;
  }

}
