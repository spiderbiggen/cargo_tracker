import {Column, Entity, ManyToOne, PrimaryColumn} from 'typeorm';
import {Vehicle} from './Vehicle';

/**
 * An entity that holds information about linked cargo has a relation with a specific vehicle.
 *
 * @author Stefan Breetveld
 */
@Entity()
export class Cargo {

  constructor(vehicle: Vehicle, uld: string, checkInTime: Date) {
    this.vehicle = vehicle;
    this.uld = uld;
    this.checkInTime = checkInTime;
  }

  @ManyToOne(() => Vehicle, vehicle => vehicle.cargo, {eager: true})
  vehicle: Vehicle;
  @PrimaryColumn()
  uld: string;
  @PrimaryColumn()
  checkInTime: Date;
  @Column('datetime', {nullable: true})
  checkOutTime?: Date | null;

  toJSON() {
    return {
      palletCarId: this.vehicle.sensorID,
      uld: this.uld,
      checkInTime: this.checkInTime,
      checkOutTime: this.checkOutTime
    };
  }
}
