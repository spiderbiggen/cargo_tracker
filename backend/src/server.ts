import {Request, Response} from 'express';
import * as TypeOrm from 'typeorm';
import 'reflect-metadata';
import {ROUTE_REGISTRY} from './routing';
import {LIST as routes} from './api/routes';
import {NotFoundError, ResponseHelper} from './util';

const bodyParser = require('body-parser');
const express = require('express');
const compression = require('compression');
const helmet = require('helmet');
const cors = require('cors');

const port = process.env.PORT || 3000;
const prodEnv = process.env.NODE_ENV === 'production';

/**
 * Create a database connection and then load all routes. Then create the server.
 *
 * @author Stefan Breetveld
 */
TypeOrm.createConnection().then(() => ROUTE_REGISTRY.load(routes).then(() => {
  const app = express();
  app.use(compression());
  app.use(helmet());
  app.use(cors());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({extended: true}));

  app.set('json replacer', (key: any, value: any) => {
    if (!value) {
      return undefined;
    }
    return value;
  });
  app.set('json spaces', prodEnv ? 0 : 2);

  const root = process.env.NODE_ROOT || '';
  ROUTE_REGISTRY.registerRoutes(app, root);
  app.use((req: Request, res: Response) => ResponseHelper.sendError(req, res, new NotFoundError()));

  if (prodEnv) {
    const fs = require('fs');
    const https = require('https');

    const certDir = process.env.CERTIFICATE_DIR;
    // Certificate
    const privateKey = fs.readFileSync(`${certDir}privkey.pem`, 'utf8');
    const certificate = fs.readFileSync(`${certDir}cert.pem`, 'utf8');
    const ca = fs.readFileSync(`${certDir}chain.pem`, 'utf8');

    const credentials = {
      key: privateKey,
      cert: certificate,
      ca: ca
    };
    https.createServer(credentials, app).listen(port);
  } else {
    app.listen(port);
  }
  console.log(`ACA Backend Running on ${port}${root}`);
})).catch(err => {
  console.log(err);
});

