import {RouteWrapper} from './RouteWrapper';
import {Express} from 'express';

/**
 * Helper class for registering all the routes in the main server file.
 *
 * @author Stefan Breetveld
 */
class RouteRegistry {
  private readonly routes: RouteWrapper[] = [];

  /**
   * Add the given RouteWrappers to the current list of route wrappers that need to be registered.
   *
   * @param wrappers All wrappers that need to be registered
   */
  async load(wrappers: RouteWrapper[]) {
    wrappers.forEach(routes => this.add(routes));
  }

  /**
   * Register the stored routes to an express app.
   *
   * @param app the express app.
   * @param route the relative route to these routes.
   */
  registerRoutes(app: Express, route?: string): void {
    this.routes.forEach(router => router.registerRoutes(app, route));
  }

  /**
   * Add the given RouteWrappers to the current list of route wrappers that need to be registered.
   *
   * @param wrapper the wrappers that needs to be registered
   */
  add(wrapper: RouteWrapper) {
    this.routes.push(wrapper);
  }
}

/**
 * Export only one instance so that we always have access to routes registered at any time.
 */
export const ROUTE_REGISTRY = new RouteRegistry();
