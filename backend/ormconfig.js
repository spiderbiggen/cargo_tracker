/**
 * TODO: Change the username and password
 * Maybe change the host and port as well
 */
module.exports = {
  "type": "mysql",
  "host": "localhost",
  "port": 3306,
  "username": "root",
  "password": "new_password",
  "database": "aca",
  "entities": [
    "./dist/api/models/**/*.js"
  ],
  "timezone": "Z",
  "synchronize": false,
  "logging": false,
  "cache": true
};
