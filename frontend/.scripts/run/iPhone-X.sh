#!/usr/bin/env bash

# Boots iPhone X
xcrun simctl boot '8EFB9F0E-7121-43D1-9901-FB14CED018B8'
xcrun simctl openurl booted 'http://localhost:4200'

# Opens Simulator
open -a 'Simulator'
