import {Component, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../_services/auth.service';
import {User} from '../_models';
import {NavService} from '../_services/nav.service';
import {BehaviorSubject} from 'rxjs';

/**
 * Controller for the login view template.
 *
 * @author Stefan Breetveld
 */
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  usernameFormControl = new FormControl('', [Validators.required]);
  passwordFormControl = new FormControl('', [Validators.required]);

  hide = true;
  loggingIn = false;
  loginFailure: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(private router: Router, private route: ActivatedRoute, private authService: AuthService, private navService: NavService) {
  }

  ngOnInit() {
    this.navService.setPageTitle('Login');

    const roles = localStorage.getItem('token');
    if (roles) {
      this.authService.confirmUser().subscribe(user => {
        if (user) {
          this.navigateToUserHome(user);
        }
      });
    }
  }

  /**
   * Send the user to the first url in the nav service for this user.
   *
   * @param user the user that just logged in
   */
  navigateToUserHome(user: User) {
    const isRoot = location.pathname === '/';
    if (isRoot) {
      const navs = this.navService.getNavsForUser(user);
      if (navs) {
        this.router.navigateByUrl(navs[0].uri);
      }
    }
  }

  /**
   * Use the auth service to log in the user with the entered username and password.
   */
  public login() {
    this.loggingIn = true;
    const loginUser = this.authService.loginUser(this.usernameFormControl.value, this.passwordFormControl.value);
    return loginUser.subscribe(user => {
      if (user) {
        this.navigateToUserHome(user);
        return;
      }
      this.loginFailure.next(true);
      this.loggingIn = false;
    }, () => {
      this.loginFailure.next(true);
      this.loggingIn = false;
    });
  }

  /**
   * Method that creates a warning message
   * @author: Stefan Breetveld
   */
  public getPasswordErrorMessage(): string {
    return this.passwordFormControl.hasError('required') ? 'Please enter a password' : '';
  }

  /**
   * Method that creates a warning message
   * @author: Stefan Breetveld
   */
  public getUsernameErrorMessage(): string {
    return this.usernameFormControl.hasError('required') ? 'Please enter a username' : '';
  }
}


