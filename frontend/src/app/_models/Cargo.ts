export class Cargo {
  palletCarId: string;
  uld: string;
  checkInTime: Date;
  checkOutTime?: Date;

  public constructor(palletCarId: string, uld: string, checkInTime: Date) {
    this.palletCarId = palletCarId;
    this.uld = uld;
    this.checkInTime = checkInTime;
  }
}
