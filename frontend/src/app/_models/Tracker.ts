export interface Tracker {
  id: number;
  sensorID: string;
  eventTime: Date;
  eventID?: string;
  gsEquipmentID?: string;
  latitude: number;
  longitude: number;
  elevation?: number;
  data: { [key: string]: any };
}

