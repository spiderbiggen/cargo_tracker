export class NavItem {
  uri: string;
  exact: boolean;
  title: string;


  constructor(uri: string, title: string, exact: boolean = false) {
    this.uri = uri;
    this.title = title;
    this.exact = exact;
  }
}
