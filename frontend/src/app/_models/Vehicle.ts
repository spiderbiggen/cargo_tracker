import {Tracker} from './Tracker';
import {Cargo} from './Cargo';

export class Vehicle {

  constructor(id: string, name: string) {
    this.sensorID = id;
    this.name = name;
  }

  sensorID: string;
  name?: string;
  gsEquipmentID?: string;
  location?: Tracker;
  cargo?: Cargo[];

  addCargo(obj: Cargo) {
    if (!this.cargo) {
      this.cargo = [];
    }
    this.cargo.push(obj);
  }
}
