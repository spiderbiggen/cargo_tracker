import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {ScanVehicleComponent} from 'src/app/driver/scan-vehicle/scan-vehicle.component';
import {VehicleViewComponent} from 'src/app/driver/vehicle-view/vehicle-view.component';
import {MapViewComponent} from 'src/app/desk/map-view/map-view.component';
import {LoginComponent} from './login/login.component';
import {AdminGuard} from './_guards/admin.guard.service';
import {EditUserComponent} from './admin/edit-user/edit-user.component';
import {ManageUsersComponent} from './admin/manage-users/manage-users.component';
import {LogoutComponent} from './logout/logout.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'scan',
    component: ScanVehicleComponent
  },
  {
    path: 'vehicle/:id',
    component: VehicleViewComponent
  },
  {
    path: 'users/edit/:id',
    canActivate: [AdminGuard],
    component: EditUserComponent
  },
  {
    path: 'users/create',
    canActivate: [AdminGuard],
    component: EditUserComponent,
  },
  {
    path: 'users',
    canActivate: [AdminGuard],
    component: ManageUsersComponent
  },
  {
    path: 'map',
    component: MapViewComponent
  },
  {
    path: 'logout',
    component: LogoutComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})

export class AppRoutingModule {
}
