import {Component, OnInit} from '@angular/core';
import {AuthService} from './_services/auth.service';
import {NavService} from './_services/nav.service';
import {NavItem, User} from './_models';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  loggedIn: User = undefined;
  nav: NavItem[] = undefined;

  title = null;
  icon = null;
  iconFunction = null;

  constructor(private userService: AuthService, private navService: NavService) {
  }

  callMethod() {
    if (this.iconFunction) {
      this.iconFunction();
    }
  }

  ngOnInit(): void {
    this.navService.pageTitle$.subscribe(title => {
      this.title = title;
    });

    this.userService.loggedInUser.subscribe(user => {
      this.nav = this.navService.getNavsForUser(user);
      this.loggedIn = user;
    });
    this.userService.confirmUser();
  }

  logout() {
    this.userService.logoutUser();
  }

  changeOfRoutes() {
    this.title = null;
    this.icon = null;
  }

}
