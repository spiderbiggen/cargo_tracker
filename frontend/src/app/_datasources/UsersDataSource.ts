import {DataSource} from '@angular/cdk/table';
import {User} from '../_models';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {CollectionViewer} from '@angular/cdk/collections';
import {UserService} from '../_services/user.service';
import {catchError, finalize} from 'rxjs/operators';
import {Output} from '@angular/core';

export class UsersDataSource implements DataSource<User> {
  private usersSubject = new BehaviorSubject<User[]>([]);
  @Output() countSubject = new BehaviorSubject<number>(0);
  @Output() loadingSubject = new BehaviorSubject<boolean>(false);

  constructor(private userService: UserService) {
  }

  connect(collectionViewer: CollectionViewer): Observable<User[]> {
    return this.usersSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.usersSubject.complete();
    this.loadingSubject.complete();
    this.countSubject.complete();
  }

  loadUsers(roles: string[] = [], pageNumber: number = 0, pageSize: number = 10, minYear?: number, maxYear?: number): void {
    this.loadingSubject.next(true);
    this.userService.getUsers(pageNumber * pageSize, pageSize).pipe(
      catchError(() => of({count: 0, users: []})),
      finalize(() => this.loadingSubject.next(false))
    ).subscribe(value => {
      this.usersSubject.next(value.users);
      this.countSubject.next(value.count);
    });
  }

}
