import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {User} from '../../_models';
import {UserService} from '../../_services/user.service';
import {MatPaginator} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';
import {UsersDataSource} from '../../_datasources/UsersDataSource';
import {tap} from 'rxjs/operators';
import {BreakpointObserver} from '@angular/cdk/layout';
import {NavService} from '../../_services/nav.service';

@Component({
  selector: 'app-manage-users',
  templateUrl: './manage-users.component.html',
  styleUrls: ['./manage-users.component.scss']
})
export class ManageUsersComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource: UsersDataSource;
  isSmallScreen: boolean;
  pageSize = 10;
  displayedColumns: string[] = ['username', 'email', 'created', 'roles'];

  constructor(private userService: UserService, private router: Router, private navService: NavService,
              private route: ActivatedRoute, private breakpointObserver: BreakpointObserver) {
    this.isSmallScreen = breakpointObserver.isMatched('(max-width: 599px)');
    if (this.isSmallScreen) {
      this.pageSize = 5;
      this.displayedColumns = ['username', 'roles'];
    }
  }

  ngOnInit() {
    this.navService.setPageTitle('Manage Users');
    this.dataSource = new UsersDataSource(this.userService);
    this.loadUsersPage();
  }

  ngAfterViewInit() {
    this.paginator.page.pipe(
      tap(() => this.loadUsersPage())
    ).subscribe();
  }

  private loadUsersPage() {
    this.dataSource.loadUsers(undefined, this.paginator.pageIndex, this.paginator.pageSize);
  }

  onRowClicked(row: User) {
    this.router.navigate(['edit', row.id], {relativeTo: this.route, replaceUrl: false});
  }

  createClicked() {
    this.router.navigate(['create'], {relativeTo: this.route, replaceUrl: false});
  }
}


