import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {User} from '../../_models';
import {ActivatedRoute} from '@angular/router';
import {UserService} from '../../_services/user.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NavService} from '../../_services/nav.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {

  readonly roles = ['admin', 'driver', 'desk'];

  create = false;
  user: User;
  id: string;
  formGroup: FormGroup;
  hide = true;

  constructor(private route: ActivatedRoute, private userService: UserService, private navService: NavService,
              private formBuilder: FormBuilder, private location: Location) {
    const snapshot = this.route.snapshot;
    this.id = snapshot.paramMap.get('id');
    this.create = !Boolean(this.id);
  }

  ngOnInit() {
    this.navService.setPageTitle('Edit User');
    this.formGroup = this.formBuilder.group({
      username: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      roles: [[]],
      created: new FormControl({value: undefined, disabled: true}),
      password: [null]
    });
    if (!this.create) {
      this.getUser(this.id);
    }
  }

  private getUser(id: string) {
    console.log(id);
    this.userService.getUser(id).subscribe(user => this.setUser(user));
  }

  private setUser(user: User) {
    this.user = user;
    this.navService.setPageTitle(`Edit ${user.username}`);
    const time = new Date(user.createdAt).toISOString().replace('Z', '');
    this.formGroup.setValue({
      username: user.username,
      email: user.email,
      roles: user.roles,
      created: time,
      password: null
    });
  }

  public backClicked() {
    this.location.back();
  }

  public saveClicked() {
    const user = this.formGroup.getRawValue();
    user['id'] = this.id;
    this.userService.updateUser(user, this.create).subscribe(() => this.location.back());
  }

  generatePassword() {
    this.formGroup.get('password').setValue('December2018');
  }
}
