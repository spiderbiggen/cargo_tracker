import {Component, OnInit} from '@angular/core';
import {AuthService} from '../_services/auth.service';

/**
 * Allow the user to log out by clicking a router link.
 *
 * @author Stefan Breetveld
 */
@Component({
  template: ''
})
export class LogoutComponent implements OnInit {

  constructor(private userService: AuthService) {
  }

  ngOnInit() {
    this.userService.logoutUser();
  }

}
