import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

/**
 * Map successful response to a normal object as it wil be returned as
 * {
 *     "status": "success",
 *     "data": <Object>
 * }
 *
 * @author Stefan Breetveld
 */
@Injectable()
export class DataWrapperInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(map(event => {
      if (event instanceof HttpResponse && event.body !== null && event.body !== undefined) {
        const status = event.body['status'];
        if (status === 'success') {
          event = event.clone({body: event.body['data']});
        }
      }
      return event;
    }));
  }
}
