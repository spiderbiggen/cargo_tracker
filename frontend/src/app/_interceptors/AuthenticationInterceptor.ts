import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Router} from '@angular/router';

/**
 * Add the authentication header to all requests to make sure we can always access the resources we are allowed to access.
 *
 * @author Stefan Breetveld
 */
@Injectable()
export class AuthenticationInterceptor implements HttpInterceptor {

  constructor(private router: Router) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = localStorage.getItem('token');
    if (token) {
      req = req.clone({setHeaders: {Authorization: `bearer ${token}`}});
    }
    return next.handle(req).pipe(map(event => {
      if (event instanceof HttpResponse) {
        if ((<HttpResponse<any>>event).status === 401) {
          localStorage.removeItem('token');
          localStorage.removeItem('roles');
          this.router.navigateByUrl('/');
        }
      }
      return event;
    }));
  }
}
