import {Component, OnInit} from '@angular/core';
import {Vehicle} from '../../_models';
import {VehicleService} from '../../_services/vehicle.service';
import {NavService} from '../../_services/nav.service';

@Component({
  selector: 'app-map-view',
  templateUrl: './map-view.component.html',
  styleUrls: ['./map-view.component.scss']
})

/**
 * Class that handles the map overview.
 * @author: Kerim Birlik, Stefan Breetveld
 */
export class MapViewComponent implements OnInit {

  // initial zoom level
  zoom = 15;

  // initial center position for the map currently set on schiphol airport
  lat = 52.306523;
  lng = 4.754778;

  // array of markers when in normal state.
  markers: Marker[] = [];

  // markers used for filtering
  selectedMarkers: Vehicle[];
  selectedType: string;

  /**
   * Method that returns the correct icon for the marker used in updateMarkers().
   * @param [v] the marker that needs the icon type.
   * @return [string] the url of the icon file
   */
  static getIconForVehicle(v: Marker): string {
    let url;
    if (v.cargo.length > 0) {
      url = './assets/icn/icon-package.svg';
    } else if (v.gsEquipmentID.toLowerCase().startsWith('hum car')) {
      url = './assets/icn/icon-hum-car.svg';
    } else {
      url = './assets/icn/icon-pallet-car.svg';
    }
    return url;
  }

  /**
   * Method that sorts the cargo
   *
   * @param a first vehicle
   * @param b second vehicle
   */
  private static sortVehicles(a: Vehicle, b: Vehicle) {
    let comp = 0;
    if (a.cargo || b.cargo) {
      comp = a.cargo ? b.cargo ? b.cargo.length - a.cargo.length : 1 : -1;
    }
    if (comp !== 0) { return comp; }
    if (a.gsEquipmentID || b.gsEquipmentID) {
      comp = a.gsEquipmentID ? b.gsEquipmentID ? a.gsEquipmentID.localeCompare(b.gsEquipmentID) : 1 : -1;
    }
    if (comp !== 0) { return comp; }
    return a.sensorID.localeCompare(b.sensorID);
  }

  constructor(private vehicleService: VehicleService, private navService: NavService) {
  }

  ngOnInit(): void {
    this.navService.setPageTitle('Map Overview');
    this.filterOwner();
  }

  /**
   * Method that updates the markers with the correct icon and color according to its catagory.
   * @param [markers] array of markers that need updated icons.
   */
  updateMarkers(markers: Marker[]) {
    this.selectedMarkers = null;
    for (const m of markers) {

      m.url = MapViewComponent.getIconForVehicle(m);
      m.scaledSize = {
        height: 55,
        width: 55
      };
      m.expanded = false;
    }
    this.markers = markers.sort(MapViewComponent.sortVehicles);
  }

  /**
   * Method that filters the markers based on the vehicle type
   * @param [filter] the string that is used to filter the markers on.
   */
  filterVehicleType(filter: string): void {
    if (this.selectedType === filter) {
      return;
    }
    this.selectedType = filter;
    this.vehicleService.getCurrentForKeyValue('vehicle_type', filter).subscribe(trackers => this.updateMarkers(trackers));
  }

  /**
   * Method that filters the markers based on the vehicle owner
   */
  filterOwner(): void {
    const filter = 'cargo';
    if (this.selectedType === filter) {
      return;
    }
    this.selectedType = filter;
    this.vehicleService.getCurrentForKeyValue('vehicle_owner', filter).subscribe(trackers => this.updateMarkers(trackers));
  }

  /**
   * Method that adds a marker to a new array after which only those markers are shown on the map.
   @param marker the marker that fired the focus action.
   */
  focusOnMarker(marker: Marker): void {
    if (!this.selectedMarkers) {
      this.selectedMarkers = [];
    }
    const index = this.selectedMarkers.indexOf(marker);
    if (index < 0) {
      this.selectedMarkers.push(marker);
    } else {
      this.selectedMarkers.splice(index, 1);
    }
    if (this.selectedMarkers.length === 0) {
      this.selectedMarkers = null;
    }
  }

  /**
   * Method that returns the primary color of our material design for the button.
   * @param type
   */
  getButtonColor(type: string): string {
    return this.selectedType === type ? 'primary' : 'none';
  }

  filterLoaded(filter: string): void {
    if (!filter) {
      this.selectedMarkers = null;
      return;
    }
    filter = filter.toLowerCase();
    this.selectedMarkers = [];
    for (const m of this.markers) {
      if (m.sensorID.toLowerCase().includes(filter) || (m.gsEquipmentID && m.gsEquipmentID.toLowerCase().includes(filter))) {
        this.selectedMarkers.push(m);
      } else {
        for (const c of m.cargo) {
          if (c.uld.toLowerCase().includes(filter)) {
            this.selectedMarkers.push(m);
            break;
          }
        }
      }
    }
  }

  /**
   * Method that gets the icon for vehicle type in the list of cargo
   *
   * @param m a map marker
   */
  getSvgForVehicle(m: Marker) {
    if (m.cargo.length > 0) {
      return 'package';
    } else if (m.gsEquipmentID.toLowerCase().startsWith('hum car')) {
      return 'hum-car';
    } else {
      return 'pallet-car';
    }
  }
}

/**
 * Interface for the marker
 */
interface Marker extends Vehicle {
  url?: string;
  scaledSize?: any;
  expanded?: boolean;
}


