import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';

import {CargoService} from '../../_services/cargo.service';
import {VehicleService} from '../../_services/vehicle.service';
import {NavService} from '../../_services/nav.service';

import {Cargo, Vehicle} from '../../_models';
import {configQuagga} from '../quagga';

import Quagga from 'quagga';

@Component({
  selector: 'app-vehicle-view',
  templateUrl: './vehicle-view.component.html',
  styleUrls: ['../driver.scss']
})
export class VehicleViewComponent implements OnInit {
  private sub: any;

  vehicle: Vehicle;
  vehicleID: string;
  cargoCodeFormControl = new FormControl('', Validators.required);

  quaggaIsActive = false;
  barcode = '';
  configQuagga: any[];

  constructor(private route: ActivatedRoute, private location: Location, private ref: ChangeDetectorRef,
              private vehicleService: VehicleService, private cargoService: CargoService,
              private router: Router, private navService: NavService) {
    Object.assign(this, {configQuagga});
  }

  /**
   * @author: René Kok
   */
  ngOnInit() {
    this.vehicle = new Vehicle('', '');
    this.sub = this.route.params.subscribe(params => {
      const id = params['id'];
      this.getVehicle(id);
      this.startScanner();
    });
  }

  /**
   * Method for receiving the vehicle details and cargo
   * @author: René Kok
   */
  private getVehicle(id: string) {
    this.vehicleService.getSensorID(id).subscribe(data => {
      this.vehicle = data;
      this.vehicleID = this.vehicle.sensorID;
      this.navService.setPageTitle(this.vehicle.gsEquipmentID);
    });
  }

  /**
   * Method for reloading the vehicle data after an change
   * @author: René Kok
   */
  private reloadCurrentVehicle() {
    this.vehicleService.getSensorID(this.vehicle.sensorID).subscribe(data => {
      this.vehicle = data;
    });
  }

  /**
   * Add cargo to vehicle
   * @author: René Kok
   */
  private addVehicleCargo(palletCarID: string, uld: string) {
    this.cargoService.addCargo(new Cargo(palletCarID, uld, new Date())).subscribe(
      res => {
        console.log(res);
        this.cargoCodeFormControl.reset();
        this.reloadCurrentVehicle();
      },
      err => {
        if (err.status === 500) {
        }
      }
    );
  }

  /**
   * Method that adds the specified bar code
   * @author: René Kok
   */
  public addCargoCodeFromFormField(): void {
    this.cargoCodeFormControl.markAsTouched();
    if (this.cargoCodeFormControl.valid) {
      this.addVehicleCargo(this.vehicle.sensorID, this.cargoCodeFormControl.value);
    }
  }

  /**
   * Method for removing cargo from the loaded vehicle
   * @author: René Kok
   */
  public removeCargoItem(cargoIndex: number): void {
    this.cargoService.unlinkCargo(this.vehicle.sensorID, this.vehicle.cargo[cargoIndex].uld).subscribe(
      res => {
        console.log(res);
        this.barcode = '';
        this.reloadCurrentVehicle();
      },
      err => {
        console.log(err.error);
      }
    );
    this.cargoCodeFormControl.reset();
  }

  /**
   * Method that clears all the cargo from the currently loaded vehicle.
   * It asks for confirmation first.
   * @author: René Kok
   */
  public clearCargo(): void {
    console.log('log');
    if (confirm('Are you sure you want to remove all the cargo from this vehicle?')) {
      this.cargoService.unlinkAllCargo(this.vehicle.sensorID).subscribe(
        res => {
          console.log(res);
          this.reloadCurrentVehicle();
        },
        err => {
          console.log(err.error);
        }
      );
    }

    this.cargoCodeFormControl.reset();
  }

  /**
   * Method that goes back to the scan vehicle page.
   * @author: René Kok
   */
  public goBack(): void {
    this.router.navigate([`/scan`]);
  }

  /**
   * Method that creates a warning message.
   * @author: René Kok
   */
  public getErrorMessage(): string {
    return this.cargoCodeFormControl.hasError('cargoCodePattern') ? 'The entered cargo code is invalid' : '';
  }

  /**
   * Method that starts the quagga controller
   * @author: René Kok
   */
  startScanner() {
    this.barcode = '';
    this.ref.detectChanges();

    Quagga.onProcessed((result) => this.onProcessed(result));

    Quagga.onDetected((result) => this.logCode(result));

    Quagga.init(this.configQuagga, (err) => {
      if (err) {
        return console.log(err);
      }
      Quagga.start();
      this.quaggaIsActive = true;
      console.log('Barcode: initialization finished. Ready to start');
    });

  }

  /**
   * Method that creates the barcode scanner overlay
   */
  private onProcessed(result: any) {
    const drawingCtx = Quagga.canvas.ctx.overlay;
    const drawingCanvas = Quagga.canvas.dom.overlay;

    if (result) {
      if (result.boxes) {
        drawingCtx.clearRect(0, 0, parseInt(drawingCanvas.getAttribute('width'), 10), parseInt(drawingCanvas.getAttribute('height'), 10));
        result.boxes.filter(function (box) {
          return box !== result.box;
        }).forEach(function (box) {
          Quagga.ImageDebug.drawPath(box, {x: 0, y: 1}, drawingCtx, {color: 'green', lineWidth: 2});
        });
      }

      if (result.box) {
        Quagga.ImageDebug.drawPath(result.box, {x: 0, y: 1}, drawingCtx, {color: '#00F', lineWidth: 2});
      }

      if (result.codeResult && result.codeResult.code) {
        Quagga.ImageDebug.drawPath(result.line, {x: 'x', y: 'y'}, drawingCtx, {color: 'red', lineWidth: 3});
      }
    }
  }

  /**
   * Writes the scanned barcode to the vehicle form control
   * @author: René Kok
   */
  private logCode(result) {
    const code = result.codeResult.code;

    console.log(code);
    this.barcode = code;
    this.cargoCodeFormControl.setValue(this.barcode);
  }
}
