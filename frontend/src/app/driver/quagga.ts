/**
 * Settings for Quagga (barcode scanner)
 * @author René Kok
 */
export const configQuagga = {
  inputStream: {
    name: 'Live',
    type: 'LiveStream',
    target: '#inputBarcode',
    constraints: {
      width: 1920,
      height: 1080,
      aspectRatio: {min: 1, max: 100},
      facingMode: 'environment'
    },
    singleChannel: false // true: only the red color-channel is read
  },
  locator: {
    patchSize: 'medium',
    halfSample: true
  },
  locate: true,
  numOfWorkers: navigator.hardwareConcurrency,
  decoder: {
    readers: [
      'code_39_reader',
      'code_128_reader'
    ]
  }
};
