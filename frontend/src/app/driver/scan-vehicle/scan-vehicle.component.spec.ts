import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ScanVehicleComponent} from './scan-vehicle.component';

describe('ScanVehicleComponent', () => {
  let component: ScanVehicleComponent;
  let fixture: ComponentFixture<ScanVehicleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ScanVehicleComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScanVehicleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
