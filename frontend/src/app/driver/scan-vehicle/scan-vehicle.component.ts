import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {AbstractControl, FormControl} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';

import {Location} from '@angular/common';

import {NavService} from '../../_services/nav.service';
import {VehicleService} from '../../_services/vehicle.service';
import {configQuagga} from '../quagga';

import Quagga from 'quagga';

@Component({
  selector: 'app-scan-vehicle',
  templateUrl: './scan-vehicle.component.html',
  styleUrls: ['../driver.scss']
})
export class ScanVehicleComponent implements OnInit, OnDestroy {
  public vehicleCodeFormControl = new FormControl('', [], [this.checkExistence.bind(this)]);

  configQuagga: any [];
  quaggaIsActive = false;
  barcode = '';

  public constructor(private router: Router, private route: ActivatedRoute, private location: Location,
                     private ref: ChangeDetectorRef, private navService: NavService,
                     private vehicleService: VehicleService) {
    Object.assign(this, {configQuagga});
  }

  /**
   * @author: René Kok
   */
  public ngOnInit() {
    this.navService.setPageTitle('Scan vehicle');
    this.startScanner();
  }

  /**
   * Method that clears the form field component
   * @author: René Kok
   */
  public clearVehicleFormField(evt: MouseEvent): void {
    if (this.vehicleCodeFormControl.value.toString() !== '') {
      // Prevents keyboard from showing up when clicked
      evt.stopPropagation();
    }

    this.vehicleCodeFormControl.reset();
  }

  /**
   * Method that loads the scanned vehicle
   * @author: René Kok
   */
  public goToNextPage(): void {
    if (!this.vehicleCodeFormControl.pending && this.vehicleCodeFormControl.valid) {
      this.router.navigate(['/vehicle', this.vehicleCodeFormControl.value]);
    }
  }

  /**
   * Method that creates a warning message
   * @author: René Kok
   */
  public getErrorMessage(): string {
    return this.vehicleCodeFormControl.hasError('required') ? 'Please enter a vehicle code' :
      this.vehicleCodeFormControl.hasError('vehicleCodePattern') ? 'The entered vehicle code is invalid' : '';
  }

  /**
   * Method that starts the quagga controller
   * @author: René Kok
   */
  startScanner() {
    this.barcode = '';
    this.ref.detectChanges();

    Quagga.onProcessed((result) => this.onProcessed(result));

    Quagga.onDetected((result) => this.logCode(result));

    Quagga.init(this.configQuagga, (err) => {
      if (err) {
        return console.log(err);
      }
      Quagga.start();
      this.quaggaIsActive = true;
      console.log('Barcode: initialization finished. Ready to start');
    });
  }

  /**
   * Writes the scanned barcode to the vehicle form control
   * @author: René Kok
   */
  private logCode(result) {
    this.barcode = result.codeResult.code;
    this.vehicleCodeFormControl.setValue(this.barcode);
  }

  /**
   * Check if vehicle/tracker-id exist
   * @author René Kok, Stefan Breetveld
   */
  private checkExistence(control: AbstractControl) {
    const obs = this.vehicleService.checkIfVehicleExist(control.value);
    obs.subscribe();
    return obs.toPromise().then(bool => bool === true ? null : {vehicleCodePattern: true}).catch(() => {
      return {vehicleCodePattern: true};
    });
  }

  /**
   * Cleanup method when instance gets destroyed
   * @author: René Kok
   */
  ngOnDestroy(): void {
    Quagga.stop();
  }

  /**
   * Method that creates the barcode scanner overlay
   */
  private onProcessed(result: any) {
    const drawingCtx = Quagga.canvas.ctx.overlay;
    const drawingCanvas = Quagga.canvas.dom.overlay;

    if (result) {
      if (result.boxes) {
        drawingCtx.clearRect(0, 0, parseInt(drawingCanvas.getAttribute('width'), 10), parseInt(drawingCanvas.getAttribute('height'), 10));
        result.boxes.filter(function (box) {
          return box !== result.box;
        }).forEach(function (box) {
          Quagga.ImageDebug.drawPath(box, {x: 0, y: 1}, drawingCtx, {color: 'green', lineWidth: 2});
        });
      }

      if (result.box) {
        Quagga.ImageDebug.drawPath(result.box, {x: 0, y: 1}, drawingCtx, {color: '#00F', lineWidth: 2});
      }

      if (result.codeResult && result.codeResult.code) {
        Quagga.ImageDebug.drawPath(result.line, {x: 'x', y: 'y'}, drawingCtx, {color: 'red', lineWidth: 3});
      }
    }
  }
}
