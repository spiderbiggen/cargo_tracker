import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthService} from '../_services/auth.service';
import {User} from '../_models';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {

  constructor(private authService: AuthService) {
  }

  static isAdmin(user: User): boolean {
    if (user.roles) {
      for (const role of user.roles) {
        if (role === 'admin') {
          return true;
        }
      }
    }
    return false;
  }

  async checkLogin(): Promise<boolean> {
    const loggedIn = await this.authService.confirmUser().toPromise()
      .then(user => {
        return user && AdminGuard.isAdmin(user);
      })
      .catch(() => false);

    if (loggedIn) {
      return true;
    }
    // Navigate to the login page with extras
    this.authService.logoutUser();
    return false;
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.checkLogin();
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.canActivate(route, state);
  }
}
