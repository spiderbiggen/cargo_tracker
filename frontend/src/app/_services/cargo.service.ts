import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Cargo} from '../_models';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {share} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class CargoService {

  constructor(private httpClient: HttpClient) {
  }

  getAllCargo(): Observable<Cargo[]> {
    const url = `${environment.apiUrl}/cargo/`;
    return this.httpClient.get<Cargo[]>(url).pipe(share());
  }

  getCargo(sensorID: string): Observable<Cargo[]> {
    const url = `${environment.apiUrl}/cargo/${sensorID}`;
    return this.httpClient.get<Cargo[]>(url).pipe(share());
  }

  addCargo(cargo: Cargo): Observable<Cargo> {
    const url = `${environment.apiUrl}/cargo/`;
    return this.httpClient.post<Cargo>(url, cargo).pipe(share());
  }

  unlinkCargo(palletCarId: string, uld: string): Observable<any> {
    const url = `${environment.apiUrl}/cargo/`;
    return this.httpClient.delete(url, {
      params: {
        palletCarId: palletCarId,
        uld: uld
      }
    }).pipe(share());
  }

  unlinkAllCargo(palletCarId: string): Observable<any> {
    const url = `${environment.apiUrl}/cargo/`;
    return this.httpClient.delete(url, {
      params: {
        palletCarId: palletCarId
      }
    }).pipe(share());
  }
}
