import {Injectable} from '@angular/core';
import {NavItem, User} from '../_models';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NavService {

  constructor() {
  }

  private pageTitleSource = new Subject<string>();
  pageTitle$ = this.pageTitleSource.asObservable();

  readonly adminNavs: NavItem[] = [
    new NavItem('/users', 'Manage Users'),
  ];

  readonly deskNavs: NavItem[] = [
    new NavItem('/map', 'Map Overview'),
  ];

  readonly driversNavs: NavItem[] = [
    new NavItem('/scan', 'Scan Vehicle'),
  ];

  getNavsForUser(user?: User): NavItem[] {
    const navs: NavItem[] = [];
    if (user) {
      user.roles.forEach(role => {
        switch (role) {
          case 'admin':
            this.adminNavs.forEach(i => navs.push(i));
            break;
          case 'driver':
            this.driversNavs.forEach(i => navs.push(i));
            break;
          case 'desk':
            this.deskNavs.forEach(i => navs.push(i));
            break;
        }
      });
    }
    navs.push(new NavItem('/logout', 'Log out'));
    return navs;
  }

  setPageTitle(title: string) {
    this.pageTitleSource.next(title);
  }
}
