import {EventEmitter, Injectable, Output} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../_models';
import {Router} from '@angular/router';
import {environment} from '../../environments/environment';
import {share} from 'rxjs/operators';

/**
 * Service to make managing authentication easier.
 *
 * @author Stefan Breetveld
 */
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  @Output() loggedInUser = new EventEmitter<User>();

  constructor(private httpClient: HttpClient, private router: Router) {
  }

  loginUser(username: string, password: string): Observable<User> {
    const url = `${environment.apiUrl}/authenticate/`;
    const pub = this.httpClient.post<TokenUser>(url, {username: username, password: password}).pipe(share());
    pub.subscribe(user => {
      localStorage.setItem('token', user.token);
      localStorage.setItem('roles', user.roles.join(','));
      this.loggedInUser.emit(user);
    });
    return pub;
  }

  confirmUser(): Observable<User> {
    if (localStorage.getItem('token')) {
      const url = `${environment.apiUrl}/user/`;
      const pub = this.httpClient.get<User>(url).pipe(share());
      pub.subscribe(user => {
        localStorage.setItem('roles', user.roles.join(','));
        this.loggedInUser.emit(user);
      }, () => this.logoutUser());
      return pub;
    }
  }

  logoutUser() {
    localStorage.removeItem('token');
    localStorage.removeItem('roles');
    this.loggedInUser.emit(undefined);
    this.router.navigate(['/']);
  }
}

class TokenUser extends User {
  token: string;
}
