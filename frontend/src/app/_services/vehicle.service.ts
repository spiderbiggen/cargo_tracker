import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Vehicle} from '../_models';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {share} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class VehicleService {

  constructor(private httpClient: HttpClient) {
  }

  getCurrentLocations(): Observable<Vehicle[]> {
    const url = `${environment.apiUrl}/vehicles/`;
    return this.httpClient.get<Vehicle[]>(url).pipe(share());
  }

  getSensorID(sensorID: string, length?: number): Observable<Vehicle> {
    const url = `${environment.apiUrl}/vehicles/${sensorID}`;
    let params;
    if (length) {
      params = {length: length.toString()};
    }
    return this.httpClient.get<Vehicle>(url, {params: params}).pipe(share());
  }

  getCurrentForKeyValue(key: string, value: string): Observable<Vehicle[]> {
    const url = `${environment.apiUrl}/vehicles/${key}/${value}`;
    return this.httpClient.get<Vehicle[]>(url).pipe(share());
  }

  checkIfVehicleExist(sensorID: string): Observable<boolean> {
    const url = `${environment.apiUrl}/vehicles/check/${sensorID}`;
    return this.httpClient.get<boolean>(url).pipe(share());
  }
}
