import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {User} from '../_models';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {share} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient) {
  }

  getUsers(pageIndex: number, pageSize: number): Observable<UsersCount> {
    const offset = pageIndex || 0;
    const limit = pageSize || 10;
    const url = `${environment.apiUrl}/user/many`;
    return this.httpClient.get<UsersCount>(url, {
      params: {
        offset: String(offset),
        limit: String(limit),
      }
    }).pipe(share());
  }

  getUser(id: string): Observable<User> {
    const url = `${environment.apiUrl}/user/${id}`;
    return this.httpClient.get<User>(url).pipe(share());
  }

  updateUser(user: User, create: boolean = false): Observable<User> {
    const url = `${environment.apiUrl}/user/`;
    return this.httpClient.post<User>(url, user, {params: {create: String(create)}}).pipe(share());
  }
}

export interface UsersCount {
  count: number;
  users: User[];
}
